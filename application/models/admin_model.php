<?php

class Admin_model extends CI_Model{ 

	public function __construct()
	{ 
		$this->load->database();
	}

	public function insert($table,$query)
	{ 
		$this->db->insert($table,$query);
	}

	public function get_cat_id($category)
	{
		$query = $this->db->get_where('category', array('name' => $category));
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
			return $data['id'];
		}
		return NULL;
	}

	public function get_category()
	{
		$query = $this->db->get('category');
		return $query;
	}
}
?>