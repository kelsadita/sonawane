<?php

class Collection_model extends CI_Model{ 

	public function __construct(){ 
	
		$this->load->database();
	}

	public function get_categories(){ 
	
		$this->db->select('name');
		$query = $this->db->get('category');
		return $query->result_array();
	}

	public function get_products($category){
		
		//retrieving the ID for the product category.
		
		$data = array(
	
			'name' => $category
		);

		$pid_query = $this->db->get_where('category', $data);
		$result    = $pid_query->row_array();
		$pid       = $result['id'];

		//retrieving the list of products for the particular product id.

		$pdata = array('category_id' => $pid);	
		$products_query = $this->db->get_where('products', $pdata);
		return $products_query->result_array();
	}

    public function get_product_des($category, $product_id)
    {
        if($product_id != null){

            $data = array('id' => $product_id);
            $query = $this->db->get_where('products', $data);
            return $query->row_array();
        
        }else{

            $data = array('category' => $category);
            $catid_query = $this->db->get('category', $data);
            $catid_row = $catid_query->row_array();
            $cat_id = $catid_row['id'];

            $prod_data = array('category_id' => $cat_id);
            $query = $this->db->get_where('products', $prod_data, 1);
            return $query->row_array();
        }
    }
    
	public function all_products()
	{
		$query = $this->db->get('products');
		return $query;
	}
}
?>
