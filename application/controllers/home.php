<?php
	class Home extends CI_Controller
	{ 
		public function __construct()
		{ 
			parent::__construct();
			$this->load->helper('url');
			$this->load->library('email');
			$this->load->model('collection_model');
		}
		
		public function index()
		{ 
			$data['title'] = 'Home';
			$data['categories'] = $this->collection_model->get_categories();
			$prod = $this->collection_model->all_products();
			$num = $prod->num_rows();
			$temp='';
			for($i=0;$i<$num;$i++)
			{
				$d = $prod->row_array($i);
				$temp = $temp.'"'.$d['name'].'",';
			}
			$data['products'] = $temp;
			$this->load->view('templates/header', $data);
			$this->load->view('pages/home', $data);
			$this->load->view('templates/footer', $data);
		}
		public function contact()
		{
			if(isset($_POST['submit']))
			{
				extract($_POST);
				echo $name." ".$email." ".$query;
				$this->email->from($email,$name);
				$this->email->to('nano@sonawaneleathers.com'); 
				//$this->email->cc('another@another-example.com'); 
				//$this->email->bcc('them@their-example.com'); 
				$this->email->subject('Query');
				$this->email->message($query);	
				//$this->email->send();
			}
			else
			{
				$data['title'] = 'Contact Us';
				$data['categories'] = $this->collection_model->get_categories();
				$this->load->view('templates/header', $data);
				$this->load->view('pages/contact_us', $data);
				$this->load->view('templates/footer', $data);
			}
		}

		public function searchfield()
		{
			
		}
	}
?>
