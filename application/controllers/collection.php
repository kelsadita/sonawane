<?php
	class Collection extends CI_Controller{ 
	
		public function __construct(){ 
		
			parent::__construct();
			$this->load->helper('url');
			$this->load->model('collection_model');
		
		}
		
		public function index($category = null)
		{ 
			$data['title']      = 'Collection';
			$data['selected']   = $category;
			$data['categories'] = $this->collection_model->get_categories();
			$data['products']   = $this->collection_model->get_products($category);
			$data['default_product']   = $this->collection_model->get_product_des($category, null);

			$this->load->view('templates/header', $data);
			$this->load->view('pages/collection_view', $data);
			$this->load->view('templates/footer', $data);
		}

        public function loadproduct($product_id)
        {
            $data['product'] = $this->collection_model->get_product_des(null, $product_id);
            $this->load->view('pages/product_view', $data);

        }
	}
?>
