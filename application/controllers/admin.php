<?php
	class Admin extends CI_Controller{ 
	
		public function __construct()
		{ 
			parent::__construct();
			$this->load->helper('url');
		}
		
		public function index()
		{ 
			$data['title'] = 'Admin';
			$this->load->view('pages/admin/home', $data);
		}
		public function products()
		{ 
			$data['title'] = 'Add products';
			$this->load->model('Admin_model');
			if(isset($_POST['submit']))
			{
				extract($_POST);
				$id=$this->Admin_model->get_cat_id($category);
				$query->category_id=$id;
				$query->product_code=$product_code;
				$query->name=$name;
				$query->price=$price;
				$query->description=$description;

                /* Create the config for upload library */
                /* (pretty self-explanatory) */
                $config['upload_path']   = './product_images'; /* NB! create this dir! */
                $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
                $config['max_size']      = '2097152';

                /* Load the upload library */
                $this->load->library('upload', $config);

                /* Create the config for image library */
                /* (pretty self-explanatory) */
                $configThumb                   = array();
                $configThumb['image_library']  = 'gd2';
                $configThumb['source_image']   = '';
                $configThumb['create_thumb']   = TRUE;
                $configThumb['maintain_ratio'] = TRUE;
                /* Set the height and width or thumbs */
                /* Do not worry - CI is pretty smart in resizing */
                /* It will create the largest thumb that can fit in those dimensions */
                /* Thumbs will be saved in same upload dir but with a _thumb suffix */
                /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
                $configThumb['width']        = 300;
                $configThumb['height']       = 400;
                /* Load the image library */
                $this->load->library('image_lib');

                if (!$this->upload->do_upload('image'))
                {
                    $data['title']   = 'Admin';
                    $data['error']   = array('error'=> $this->upload->display_errors());
                    $data['ref_url'] = base_url().'admin/products';
                    $this->load->view('templates/header', $data);
                    $this->load->view('pages/upload_error_view', $data);
                    $this->load->view('templates/footer');
                }
                else
                {
                    $data = $this->upload->data();
                    if($data['is_image'] == 1) 
                    {
                        $configThumb['source_image'] = $data['full_path'];
                        $this->image_lib->initialize($configThumb);
                        $this->image_lib->resize();
                    }
                    
                    //$thumb_name = $data['raw_name']."_thumb".$data['file_ext'];
                    //$image_name = $data['file_name'];
                    //$user_id = $this->session->userdata('user_id');

                    $query->image_name = $data['file_name'];

                    $this->Admin_model->insert('products',$query);

                    echo "Image uploaded successfully!";
                    //redirect('photos/view/'.$album_id);	
                }




			}
			else
			{
				$data['category']=$this->Admin_model->get_category();
				$this->load->view('pages/admin/products',$data);
			}
		}
		public function category()
		{
			$data['title'] = 'Add category';
			$this->load->model('Admin_model');
			if(isset($_POST['submit']))
			{
				extract($_POST);
				$id=$this->Admin_model->get_cat_id($category);
				if($id==NULL)
				{
					$query->name=$category;
					$this->Admin_model->insert('category',$query);
					echo "added";
				}
				else
				{
					echo "Category already exists can not be added again :)";
				}
			}
			else
			{
				$this->load->view('pages/admin/category',$data);
			}
		}
	}
?>
