<script type="text/javascript">
    $(document).ready(function(){
    
        $("#product_ul li:first-child").addClass("current");

        $(".product").click(function(){
            
            var product_id = $(this).attr('id');
            $("#product_ul li").each(function(i){
               $(this).removeClass("current"); 
            });
            $(this).addClass("current");
            var loadurl = "<?=base_url();?>collection/loadproduct/"+product_id;
            $("#product_des").load(loadurl);
        });
    });
</script>

<section>
	<hr noshade style = "border : 2px solid #CCCCCC;"/>	
	<center><h1><?=$selected?></h1></center>
	<hr noshade style = "border : 2px solid #CCCCCC;"/>	

	<table cellspacing="0" id = "products" border = "1">
		<tr>
			<td id = "product_list">
				
				<?php
						echo '<ul id="product_ul">';
						foreach ($products as $product) {
							
							echo '<li id="'.$product['id'].'" class = "product">'.$product['name'].'</li>';
						}
						echo '</ul>';
				?>
			
			</td>
		
			<td id = "product_des">
                <div>  
                    <a class="fancybox-effects-d" href="<?php echo PIMAGE_PATH.$default_product['image_name'];?>" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit"><img src="<?php echo PIMAGE_PATH.$default_product['image_name'];?>" width = "400" height = "400" alt="" /></a>
                </div>
            </td>
		</tr>
	</table>
</section>
