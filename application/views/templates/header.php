<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= $title; ?></title>
		<meta name="description" content="">
		<!-- Style sheets -->
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/nav_menu.css">
        
        <!-- Stylesheet for nivo slider -->
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/nivo_slider.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/deafult.css">

        <!-- Stylesheet for fancybox -->
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>css/jquery.fancybox.css?v=2.0.6" media="screen" />
        
        <!-- Stysheet for jquery ui -->
        <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/jquery-ui.css"  media="all" />
        <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/ui.theme.css" media="all" />

        <!--JavaScript source files -->
		<script src="<?=base_url();?>js/jquery.js"></script>
		<script src="<?=base_url();?>js/jquery-1.7.2.js"></script>
		<script src="<?=base_url();?>js/jquery.ui.core.js"></script>
		<script src="<?=base_url();?>js/jquery.ui.widget.js"></script>
		<script src="<?=base_url();?>js/jquery.ui.position.js"></script>
		<script src="<?=base_url();?>js/jquery.ui.autocomplete.js"></script>
		<script src="<?=base_url();?>js/jquery.nivo.slider.pack.js"></script>
        <script src="<?=base_url();?>js/jquery.fancybox.js?v=2.0.6"></script>
		

        <script type="text/javascript">
		    	$(document).ready(function() {
				//slider required at the home page
				$('#slider').nivoSlider({
					effect: 'sliceDown', 
        				slices: 15, // For slice animations
        				animSpeed: 1000, // Slide transition speed
        				pauseTime: 3000, // How long each slide will show
        				startSlide: 0, // Set starting Slide (0 index)
        				directionNav: true, // Next & Prev navigation
        				directionNavHide: true, // Only show on hover
        				controlNav: true, // 1,2,3... navigation
        				controlNavThumbs: false, // Use thumbnails for Control Nav
        				pauseOnHover: true, // Stop animation while hovering
        				manualAdvance: false, // Force manual transitions
                });

                //script required for the fancy box

                
                $(".fancybox-effects-d").fancybox({
                    
                    padding: 0,

                    openEffect : 'elastic',
                    openSpeed  : 150,

                    closeEffect : 'elastic',
                    closeSpeed  : 150,

                    loseClick : true,

                    helpers : {
                        overlay : null
                    }
                });
                

                //Jquery ui autocomplete plugin

				<?php

				?>
				var availableTags = [<?=$products?>];
				$( "#searchField" ).autocomplete({
					source: availableTags
				});
			});
			</script>
	</head>
	<body>
	    	<div id="content">
			<header>
				<nav>
				<div id = "nav">
				<ul id = "menu">
				    
				    <li class="logo">
						<img style="float:left;" alt="" src="<?=IMAGE_PATH?>menu_left.png"/>
        			</li>
					
					<li>
					    <a href="<?=base_url()?>home" <?php if( $title == "Home") echo 'style="color:#B0D730;"';?>>Home</a>
  					</li>

    				<li>
						<a href="#" <?php if( $title == "Collection") echo 'style="color:#B0D730;"';?>>Collection</a>
						
						<ul id="help"> 
						<?php 
						  	$i = 0;

							foreach ( $categories as $category ){
							
							if($i == 0){
		 				?> 
								<li> 
						    		<img class="corner_inset_left" alt="" src="<?=IMAGE_PATH?>corner_inset_left.png"/> 
									<a href="<?=base_url()?>collection/index/<?=$category['name']?>"><?=$category['name']?></a> 
            						<img class="corner_inset_right" alt="" src="<?=IMAGE_PATH?>corner_inset_right.png"/> 
        						</li> 
						<?php

							}else{

						?>	
								<li><a href="<?=base_url()?>collection/index/<?=$category['name']?>"><?=$category['name']?></a></li> 
        					
						<?php
							}
							$i++;
						?>	
						
						<?php } ?>	
						<li class="last"> 
            				<img class="corner_left" alt="" src="<?=IMAGE_PATH?>corner_left.png"/> 
            				<img class="middle" alt="" src="<?=IMAGE_PATH?>dot.gif"/> 
            				<img class="corner_right" alt="" src="<?=IMAGE_PATH?>corner_right.png"/> 
        				</li> 
					    </ul> 
  					</li>

					<li>
						<a href="#">About Us</a>
  					</li>
					
					<li>
						<a href="#">Enquiry</a>
  					</li>
					
					<li>
						<a href="<?=base_url()?>home/contact" <?php if( $title == "Contact Us") echo 'style="color:#B0D730;"';?>>Contact Us</a>
  					</li>

					<li class="searchContainer" style="float:right;">
        				<div>
        					<input type="text" id="searchField" />
						<img src="<?=IMAGE_PATH?>magnifier.png" alt="Search"/>
					</div>
        				</li>
					<!--<img style="float:left;" alt="" src="<?=IMAGE_PATH?>menu_right.png"/>-->
				</ul>
			    	</div>
				</nav>
			</header>
			
			<!-- Body starts after this -->


